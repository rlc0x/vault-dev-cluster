vault-dev-cluster
=========

Creates a development Hashicorp Vault cluster

Installs the latest version of Vault and configures a basic cluster using raft storage

Requirements
------------

Three Red Hat or Rocky Linux nodes 

An SSL certificate with all the node DNS names and IP addresses listed as Subject Alternative Names.

Role Variables
--------------

- **vault_primary_node:** Name of the system that will act as the main configuration node for initialization.
- **vault_storage:** Defaults to `raft`
- **vault_storage_path:** Defaults to `/opt/raft`
- **vault_port:** Defaults to `8200`
- **vault_cluster_port Defaults to `8201`
- **vault_leader_tls_servername:** Normally the DNS name of the proxy or CNAME round robin record e.g: `https://vault.example.com` 
- **vault_owner:** Defaults to `vault`
- **vault_group:** Defaults to `vault`
- **vault_config_file:** Defaults to `/etc/vault.d/vault.hcl`
- **vault_config_file_template:** Defaults to `vault_hcl.j2`
- **vault_base_dir:** Defaults to `/opt/vault`
- **vault_dev_cluster__vault_base_dir:** 
- **vault_leader_client_cert_file:** 
- **vault_leader_client_key_file:** 
- **vault_leader_ca_cert_file:** 
- **vault_log_level:** Defaults to `trace`
- **vault_cluster_name:** Defaults to `vault-cluster`
- **vault_address:** Defaults to `0.0.0.0:8200`
- **vault_cluster_address:** Defaults to `0.0.0.0:8201`
- **vault_unseal_service_script:** Defaults to `/usr/local/bin/vault-unseal.sh`
- **vault_unseal_service_script_template:** Defaults to `vault-unseal_sh.j2`
- **vault_unseal_systemd_service_definition:** Defaults to `/etc/systemd/system/vault-unseal.service`
- **vault_unseal_systemd_service_definition_template:** Defaults to `vault_service.j2`


Dependencies
------------


Example Playbook
----------------

```yaml
---
- name: Install and configure Hashicorp Vault
  hosts: all
  become: True
  vars:
    vault_primary_node: vault1
    vault_leader_tls_servername: vault.example.com
    vault_leader_client_cert_file: vault.example.crt
    vault_leader_client_key_file: vault.example.key
    vault_leader_ca_cert_file: vault.ca.crt
  tasks:
    - name: Configure the vault nodes
      include_role:
        name: vault-dev-cluster
      when: ansible_hostname in groups['vault']
```

License
-------

MIT

Author Information
------------------

Raymond L Cox
